package org.example.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SelectablePage {
    public WebDriver driver;
    public Actions actions;

    public SelectablePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Selectable']")
    private WebElement selectableMenu;
    @FindBy(xpath = "//a[.='Display as grid']")
    private WebElement displayAsGridMenu;
    @FindBy(xpath = "//*[@id='content']/iframe")
    private WebElement frameSelect;

    // test case 1
    public void clickSelectableMenu() {
        selectableMenu.click();
    }

    public void itemSelect(String itemNumber) {
        driver.switchTo().frame(frameSelect);

        String itemElement = "//li[starts-with(text(),\"" + itemNumber + "\")]";

        WebElement itemToCtrlClick = driver.findElement(By.xpath(itemElement));
        actions.keyDown(Keys.CONTROL).click(itemToCtrlClick).perform();

        driver.switchTo().defaultContent();
    }

    public boolean verifySelectedColour(String itemColour) {
        driver.switchTo().frame(frameSelect);

        WebElement itemElementColour = driver.findElement(By.xpath("//li[starts-with(text(),\"" + itemColour + "\")]"));
        String colourChangeSelected = itemElementColour.getCssValue("background");
        String expectedSelectedColour = "#F39814";

        driver.switchTo().defaultContent();

        return colourChangeSelected.equalsIgnoreCase(expectedSelectedColour);
    }

    // test case 2
    public void clickSDisplayAsGridMenu() {
        displayAsGridMenu.click();
    }

    public void groupSelect(String starts, String finishs) {
        driver.switchTo().frame(frameSelect);

        String startElement = "//li[starts-with(text(),\"" + starts + "\")]";
        WebElement startWebElement = driver.findElement(By.xpath(startElement));
        String rightElement = "//li[starts-with(text(),\"" + finishs + "\")]";
        WebElement finishWebElement = driver.findElement(By.xpath(rightElement));

        actions.clickAndHold(startWebElement).moveToElement(finishWebElement).release()
                .build().perform();

        driver.switchTo().defaultContent();
    }

}
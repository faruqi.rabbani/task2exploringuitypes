package org.example.pageObject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DatepickerPage {
    public WebDriver driver;
    public Actions actions;

    public DatepickerPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Datepicker']")
    private WebElement datepickerMenu;
    @FindBy(xpath = "//input[@id='datepicker']")
    private WebElement dateBox;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameDatepicker;
    @FindBy(xpath = "//*[@id='ui-datepicker-div']/table/tbody")
    private List<WebElement> listCalendarElement;

    public void clickDatepickerMenu() {
        datepickerMenu.click();
    }

    public void clickOnDateBox() {
        driver.switchTo().frame(frameDatepicker);

        dateBox.click();
    }

    public void selectDate(String dates) {
        String dateElement = "//td[.=\"" + dates + "\"]";
        WebElement dateWebElement = driver.findElement(By.xpath(dateElement));

        actions.click(dateWebElement).build().perform();
    }

    public boolean verifyDatepickerColour() {
        boolean loopDateElement = false;

        for (WebElement list : listCalendarElement) {
            if (list.getCssValue("background").equals("#007fff")) {
                loopDateElement = true;
                break;
            }
        }

        return loopDateElement;
    }

}
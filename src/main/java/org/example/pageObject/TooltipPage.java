package org.example.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TooltipPage {
    public WebDriver driver;
    public Actions actions;

    public TooltipPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Tooltip']")
    private WebElement tooltipMenu;
    @FindBy(xpath = "//input[@id='age']")
    private WebElement yourAgeBox;
    @FindBy(xpath = "/html/body/div/div[1]")
    private WebElement tooltipBox;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameTooltip;

    public void clickTooltipMenu() {
        tooltipMenu.click();
    }

    public void hoverThePointerToYourAgeBox() {
        driver.switchTo().frame(frameTooltip);

        actions.clickAndHold().moveToElement(yourAgeBox).click(yourAgeBox).build().perform();
        actions.moveToElement(yourAgeBox).build().perform();
    }

    public String verifyTextInsideTooltip() {
        String tooltipText = tooltipBox.getText();

        driver.switchTo().defaultContent();

        return tooltipText;
    }

}

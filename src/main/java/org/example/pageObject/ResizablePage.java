package org.example.pageObject;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResizablePage {
    public WebDriver driver;
    public Actions actions;

    public ResizablePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Resizable']")
    private WebElement resizableMenu;
    @FindBy(xpath = "//div[@class='ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se']")
    private WebElement resizableCorner;
    @FindBy(xpath = "//*[@id='content']/iframe")
    private WebElement frameResize;
    @FindBy(xpath = "//div[@id='resizable']")
    private WebElement resizableBox;

    public void clickResizableMenu() {
        resizableMenu.click();
    }

    public Dimension getSizeBeforeResize() {
        driver.switchTo().frame(frameResize);

        Dimension sizeBefore = resizableBox.getSize();

        return sizeBefore;
    }

    public void resizeAction(int heights, int widths) {
        actions.clickAndHold(resizableCorner)
                .moveByOffset(widths, heights).build().perform();
    }

    public Dimension getSizeAfterResize() {
        Dimension sizeAfter = resizableBox.getSize();

        return sizeAfter;
    }

    public boolean verifyResizeSuccess() {
        driver.switchTo().defaultContent();

        Dimension sizeBeforeResize = getSizeBeforeResize();
        Dimension sizeAfterResize = getSizeAfterResize();

        int widthBeforeResize = sizeBeforeResize.getWidth();
        int heightBeforeResize = sizeBeforeResize.getHeight();
        int widthAfterResize = sizeAfterResize.getWidth();
        int heightAfterResize = sizeAfterResize.getHeight();

        boolean hasil = false;

        if (widthBeforeResize < widthAfterResize && heightBeforeResize < heightAfterResize) {
            hasil = true;
        } else {
            hasil = false;
        }

        return hasil;
    }

}
package org.example.pageObject;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProgressbarPage {
    public WebDriver driver;
    public Actions actions;

    public ProgressbarPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Progressbar']")
    private WebElement progressbarMenu;
    @FindBy(xpath = "//a[.='Download Dialog']")
    private WebElement downloadDialogButton;
    @FindBy(xpath = "//button[@id='downloadButton']")
    private WebElement startDownloadButton;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameProgressBar;

    public void clickProgressbarMenu() {
        progressbarMenu.click();
    }

    public void clickDownloadDialog() {
        downloadDialogButton.click();
    }

    public void clicksStartDownload() {
        driver.switchTo().frame(frameProgressBar);

        startDownloadButton.click();
    }

    public boolean verifyCancelTextAndPercentage(String text, String value) {
        String percentButton = "//div[starts-with(text(),\"" + value + "\")]";
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement percentButtonElement = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(percentButton)));

        String cancelButton = "//button[starts-with(text(),\"" + text + "\")]";

        if (percentButtonElement.isDisplayed()) {
            WebElement cancelButtonElement = driver.findElement(By.xpath(cancelButton));
            return cancelButtonElement.isDisplayed();
        }

        return false;
    }

    public boolean verifyCompleteText(String completed) {
        String completeText = "//div[starts-with(text(),\"" + completed + "\")]";
        
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        WebElement completeTextElement = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(completeText)));

        return completeTextElement.isDisplayed();
    }

}

package org.example.pageObject;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccordionPage {
    public WebDriver driver;
    public Actions actions;

    public AccordionPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Accordion']")
    private WebElement accordionMenu;
    @FindBy(xpath = "//*[@id='ui-id-6']")
    private WebElement textShow;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameAccordion;

    public void clickAccordionMenu() {
        accordionMenu.click();
    }

    public void clickAccordionSection(String accSec) {
        driver.switchTo().frame(frameAccordion);

        String accordionElement = "//h3[starts-with(text(),\"" + accSec + "\")]";
        WebElement accordionWebElement = driver.findElement(By.xpath(accordionElement));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", accordionWebElement);

        accordionWebElement.click();
    }

    public void verifyText() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(textShow));

        textShow.isDisplayed();
    }

}
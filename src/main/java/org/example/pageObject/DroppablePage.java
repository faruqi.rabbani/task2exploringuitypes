package org.example.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DroppablePage {
    public WebDriver driver;
    public Actions actions;

    public DroppablePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[@href='/']")
    private WebElement landingPage;
    @FindBy(xpath = "//a[.='Droppable']")
    private WebElement droppableMenu;
    @FindBy(xpath = "//div[@id='draggable']")
    private WebElement dragObject;
    @FindBy(xpath = "//div[@id='droppable']")
    private WebElement dropPlace;
    @FindBy(xpath = "//p[.='Dropped!']")
    private WebElement droppedText;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameDrop;
    @FindBy(xpath = "//div[@id='draggable']")
    private WebElement draggableBox;
    @FindBy(xpath = "//div[@id='droppable']")
    private WebElement destinationBox;

    public boolean verifyLandingPage() {
        return landingPage.isDisplayed();
    }

    public void clickDroppableMenu() {
        droppableMenu.click();
    }

    public void dragAndDrop() {
        driver.switchTo().frame(frameDrop);

        actions.dragAndDrop(draggableBox, destinationBox).build().perform();
    }

    public boolean verifyDroppedText() {
        return droppedText.isDisplayed();
    }

    public boolean verifyDroppedColour() {
        String colorValue = dropPlace.getCssValue("background");
        String expectedColor = "#fffa90";

        return colorValue.equalsIgnoreCase(expectedColor);
    }

}
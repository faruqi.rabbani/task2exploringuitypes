package org.example.pageObject;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SortablePage {
    public WebDriver driver;
    public Actions actions;

    public SortablePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Sortable']")
    private WebElement sortableMenu;
    @FindBy(xpath = "//*[@id='content']/iframe")
    private WebElement frameSorting;
    private List<WebElement> itemsBeforeSorting;
    private List<WebElement> itemsAfterSorting;

    public void clickSortableMenu() {
        sortableMenu.click();
    }

    public void orderBeforeSorting() {
        driver.switchTo().frame(frameSorting);
        itemsBeforeSorting = driver.findElements(By.xpath("//*[@id='sortable']"));
        driver.switchTo().defaultContent();
    }

    public void performSorting(String sorting1, String sorting2) {
        driver.switchTo().frame(frameSorting);

        String sortingElementStart = "//li[starts-with(text(),\"" + sorting1 + "\")]";
        String sortingElementFinish = "//li[starts-with(text(),\"" + sorting2 + "\")]";
        WebElement startElementSort = driver.findElement(By.xpath(sortingElementStart));
        WebElement finishElementSort = driver.findElement(By.xpath(sortingElementFinish));

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(startElementSort));

        actions.clickAndHold(startElementSort)
                .dragAndDropBy(finishElementSort, 0, 50);

        actions.release().build().perform();

        driver.switchTo().defaultContent();
    }

    public void orderAfterSorting() {
        itemsAfterSorting = driver.findElements(By.xpath("//*[@id='sortable']"));
    }

    public boolean verifyNewItemOrder() {
        boolean orderChanged = !itemsBeforeSorting.equals(itemsAfterSorting);

        return orderChanged;
    }

}
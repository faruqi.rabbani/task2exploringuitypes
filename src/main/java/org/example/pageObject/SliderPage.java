package org.example.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SliderPage {
    public WebDriver driver;
    public Actions actions;

    public SliderPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Slider']")
    private WebElement sliderMenu;
    @FindBy(xpath = "//a[.='Range slider']")
    private WebElement rangeSliderButton;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameSlider;
    @FindBy(xpath = "//*[@id='slider-range']/span[1]")
    private WebElement startButtonElement;
    @FindBy(xpath = "//*[@id='slider-range']/span[2]")
    private WebElement finishButtonElement;

    public void clickSliderMenu() {
        sliderMenu.click();
    }
    public void clickRangeSlider() {
        rangeSliderButton.click();
    }

    public void performSlider(int starts, int finishs) {
        driver.switchTo().frame(frameSlider);

        actions.dragAndDropBy(startButtonElement, starts, 0).perform();
        actions.dragAndDropBy(finishButtonElement, finishs, 0).perform();

        driver.switchTo().defaultContent();
    }
    
}
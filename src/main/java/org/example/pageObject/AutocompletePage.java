package org.example.pageObject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutocompletePage {
    public WebDriver driver;
    public Actions actions;

    public AutocompletePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Autocomplete']")
    private WebElement autocompleteMenu;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameAutocomplete;
    @FindBy(xpath = "//input[@id='tags']")
    private WebElement text_box;
    @FindBy(className = "ui-menu-item")
    private List<WebElement> listElementAutocomplete;
    @FindBy(xpath = "//input[@id='tags']")
    private WebElement inputBox;

    public void clickAutocompleteMenu() {
        autocompleteMenu.click();
    }

    public void inputAutocomplete(String word) {
        driver.switchTo().frame(frameAutocomplete);

        text_box.sendKeys(word);
    }

    public void selectAutocompleteOpt(String opt) {
        for (WebElement list : listElementAutocomplete) {
            if (list.getText().equals(opt)) {
                list.click();
            }
        }
    }

    public String getInputBoxText() {
        String actualText = inputBox.getAttribute("value");

        return actualText;
    }

}

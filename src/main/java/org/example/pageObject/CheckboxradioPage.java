package org.example.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckboxradioPage {
    public WebDriver driver;
    public Actions actions;

    public CheckboxradioPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.actions = new Actions(this.driver);
    }

    @FindBy(xpath = "//a[.='Checkboxradio']")
    private WebElement checkboxradioMenu;
    @FindBy(xpath = "/html/body/div/fieldset[2]")
    private WebElement CheckboxText;
    @FindBy(xpath = "/html/body/div/fieldset[3]")
    private WebElement CheckboxNestedInLabelText;
    @FindBy(xpath = "//iframe[@class='demo-frame']")
    private WebElement frameCheckbox;

    public void clickCheckboxradioMenu() {
        checkboxradioMenu.click();
    }

    public void selectLocationCheckbox(String location) {
        driver.switchTo().frame(frameCheckbox);

        String locationElement = "//label[starts-with(text(),\"" + location + "\")]";
        WebElement locElement = driver.findElement(By.xpath(locationElement));

        actions.click(locElement).build().perform();
    }

    public void scrollPage() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView({behavior: 'auto', block: 'center'});", CheckboxText);
    }

    public void scrollPage2() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, 500);");
    }

    public void selectHotelRatingCheckbox(String rating) {
        String ratingElement = "//label[starts-with(text(),\"" + rating + "\")]";
        WebElement rateElement = driver.findElement(By.xpath(ratingElement));

        actions.click(rateElement).build().perform();
    }

    public void selectBedTypeCheckbox(String bedType) {
        String bedElement = "//label[starts-with(text(),\"" + bedType + "\")]";
        WebElement bedTypeElement = driver.findElement(By.xpath(bedElement));

        actions.click(bedTypeElement).build().perform();

        driver.switchTo().defaultContent();
    }

    // public boolean verifyCheckboxRadioColour() {
    // driver.switchTo().frame(frameCheckbox);
    // boolean loopWebElement = false;

    // List<WebElement> li = driver.findElements(
    // By.xpath("/html/body/div/fieldset[1] | /html/body/div/fieldset[2] |
    // /html/body/div/fieldset[3]"));
    // for (WebElement list : li) {
    // if (list.getCssValue("background").equals("#007fff")) {
    // loopWebElement = true;
    // break;
    // }
    // }

    // driver.switchTo().defaultContent();
    // return loopWebElement;
    // }

    public boolean verifySelectedColour(String box) {
        driver.switchTo().frame(frameCheckbox);

        String boxColourElement = "//label[starts-with(text(),\"" + box + "\")]";
        WebElement boxColourWebElement = driver.findElement(By.xpath(boxColourElement));

        String colorValue = boxColourWebElement.getCssValue("background");
        String expectedColor = "#007fff";

        driver.switchTo().defaultContent();

        return colorValue.equalsIgnoreCase(expectedColor);
    }

}
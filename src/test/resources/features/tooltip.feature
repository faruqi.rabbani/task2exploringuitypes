@Interactions
Feature: Tooltip

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select tooltip menu, hover to your age box, and validate the text inside tooltip.
        Given User already on website jQueryui.com
        When User clicks Tooltip menu
        And Hover the mouse pointer to the your age box
        Then Validate text inside tooltip is "We ask for your age only for statistical purposes."
@Interactions
Feature: Autocomplete

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select autocomplete menu, input text on the box, and validate the word.
        Given User already on website jQueryui.com
        When User clicks Autocomplete menu
        And Input "J" word
        And User select "Java" on the autocomplete option
        Then Validate the text inside input box is "Java"
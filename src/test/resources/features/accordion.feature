@Interactions
Feature: Accordion

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select accordion menu, click a section, and validate text.
        Given User already on website jQueryui.com
        When User clicks Accordion menu
        And Click "Section 3" on accordion content panel
        Then Validate text is displayed
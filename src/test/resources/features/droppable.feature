@Interactions
Feature: Droppable

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select droppable menu, and perform drag and drop.
        Given User already on website jQueryui.com
        When User clicks Droppable menu
        And Perform drag and drop to the target
        Then Dropped text will be showed up
        And Boxs colour will change to yellow
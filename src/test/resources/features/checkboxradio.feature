@Interactions
Feature: Checkboxradio

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select checkboxradio menu, and select location, rating, & bed type.
        Given User already on website jQueryui.com
        When User clicks Checkboxradio menu
        And Select "London" as location
        And Select "4 Star" as hotel ratings
        And Select "1 Queen" as bed type
        Then Validate "London" Boxs colour will change to blue
        * Validate "4 Star" Boxs colour will change to blue
        * Validate "1 Queen" Boxs colour will change to blue
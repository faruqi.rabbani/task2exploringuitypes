@Interactions
Feature: Datepicker

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select datepicker menu, and perform click on selected date.
        Given User already on website jQueryui.com
        When User clicks Datepicker menu
        And Clicks on date box
        And User select date "11" on calendar pop up
        Then Selected date box colour will change to blue
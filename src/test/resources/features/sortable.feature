@Interactions
Feature: Sortable

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select sortable menu, and perform sorting item.
        Given User already on website jQueryui.com
        When User clicks Sortable menu
        And Perform sorting item "Item 1" to the item "Item 4"
        And Also do sorting item "Item 2" to the item "Item 5"
        Then User verifies that the new item order is changed after the sorting action
@Interactions
Feature: Selectable

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, click selectable menu, and perform select on item.
        Given User already on website jQueryui.com
        When User clicks Selectable menu
        And Perform select "Item 1" on the menu
        * Perform select "Item 4" on the menu
        * Perform select "Item 3" on the menu
        * Perform select "Item 5" on the menu
        Then Color of "Item 1" will change to orange
        * Color of "Item 4" will change to orange
        * Color of "Item 3" will change to orange
        * Color of "Item 5" will change to orange

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, click selectable menu, choose display as grid, and perform select on item.
        Given User already on website jQueryui.com
        When User clicks Selectable menu
        And Select Display as Grid
        And User perform group select on the item start from "2" until "8"
@Interactions
Feature: Resizable

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select resizable menu, and perform resize the box.
        Given User already on website jQueryui.com
        When User clicks Resizable menu
        And Perform resize the box by height 40 and width 130
        Then Validate size of the box is bigger
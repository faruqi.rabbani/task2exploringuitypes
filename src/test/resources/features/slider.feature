@Interactions
Feature: Slider

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select slider menu, and perform slider to desirable number.
        Given User already on website jQueryui.com
        When User clicks Slider menu and select Range slider
        And Perform slider from -30 to 150 on price range
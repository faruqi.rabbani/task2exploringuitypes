@Interactions
Feature: Progressbar

    Scenario: As a tester I want to open interaction section menu on website jQueryiu. Start from landing page, select progressbar menu, click start download, and wait until download complete.
        Given User already on website jQueryui.com
        When User clicks Progressbar menu
        And Click download dialog
        And Click start download button
        And User validate "Cancel Download" button is displayed when "Current Progress: 50%"
        Then After download progress is finished, the "Complete!" text will be displayed
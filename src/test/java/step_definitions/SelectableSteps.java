package step_definitions;

import org.example.pageObject.SelectablePage;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SelectableSteps {

    private WebDriver driver = Hooks.driver;
    SelectablePage selectablePage = new SelectablePage(driver);

    // test case 1
    @When("User clicks Selectable menu")
    public void userClicksSelectableMenu() {
        selectablePage.clickSelectableMenu();
    }

    @And("Perform select {string} on the menu")
    public void performSelectOnFourItems(String item) {
        selectablePage.itemSelect(item);
    }

    @Then("Color of {string} will change to orange")
    public void itemsColourThatHasSelectedChangeToOrange(String item) {
        selectablePage.verifySelectedColour(item);
    }

    // test case 2
    @And("Select Display as Grid")
    public void selectDisplayAsGrid() {
        selectablePage.clickSDisplayAsGridMenu();
    }

    @And("User perform group select on the item start from {string} until {string}")
    public void performSelectByGroup(String start, String finish) {
        selectablePage.groupSelect(start, finish);
    }

}
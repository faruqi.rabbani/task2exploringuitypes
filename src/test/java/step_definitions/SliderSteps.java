package step_definitions;

import org.example.pageObject.SliderPage;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;

public class SliderSteps {

    private WebDriver driver = Hooks.driver;
    SliderPage sliderPage = new SliderPage(driver);

    @When("User clicks Slider menu and select Range slider")
    public void userClicksSliderMenuAndSelectRangeSlider() {
        sliderPage.clickSliderMenu();
        sliderPage.clickRangeSlider();
    }

    @And("Perform slider from {int} to {int} on price range")
    public void performSliderFromOneToOtherPrice(int start, int finish) {
        sliderPage.performSlider(start, finish);
    }


}


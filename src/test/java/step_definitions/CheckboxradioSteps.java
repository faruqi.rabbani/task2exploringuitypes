package step_definitions;

import org.example.pageObject.CheckboxradioPage;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CheckboxradioSteps {

    private WebDriver driver = Hooks.driver;
    CheckboxradioPage checkboxradioPage = new CheckboxradioPage(driver);

    @When("User clicks Checkboxradio menu")
    public void userClickCheckboxradioMenu() {
        checkboxradioPage.clickCheckboxradioMenu();
    }

    @And("Select {string} as location")
    public void selectCheckboxLocation(String loc) {
        checkboxradioPage.selectLocationCheckbox(loc);
        checkboxradioPage.scrollPage();
    }
    
    @And("Select {string} as hotel ratings")
    public void selectCheckboxHotelRatings(String rate) {
        checkboxradioPage.selectHotelRatingCheckbox(rate);
        checkboxradioPage.scrollPage2();
    }

    @And("Select {string} as bed type")
    public void selectCheckboxBedType(String bed) {
        checkboxradioPage.selectBedTypeCheckbox(bed);
    }

    @Then("Validate {string} Boxs colour will change to blue")
    public void boxsColourThatSelectedWillChangeToBlue(String colour) {
        checkboxradioPage.verifySelectedColour(colour);
    }

}

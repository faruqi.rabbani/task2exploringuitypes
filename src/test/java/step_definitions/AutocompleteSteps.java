package step_definitions;

import org.example.pageObject.AutocompletePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AutocompleteSteps {

    private WebDriver driver = Hooks.driver;
    AutocompletePage autocompletePage = new AutocompletePage(driver);

    @When("User clicks Autocomplete menu")
    public void userClickAutoCompleteMenu() {
        autocompletePage.clickAutocompleteMenu();
    }

    @And("Input {string} word")
    public void inputAutocompleteWord(String autoText) {
        autocompletePage.inputAutocomplete(autoText);
    }

    @And("User select {string} on the autocomplete option")
    public void userSelectOneOfAutocompleteOption(String option) {
        autocompletePage.selectAutocompleteOpt(option);
    }

    @Then("Validate the text inside input box is {string}")
    public void validateTextInsideBox(String java) {
        Assert.assertEquals(java, autocompletePage.getInputBoxText());
    }

}

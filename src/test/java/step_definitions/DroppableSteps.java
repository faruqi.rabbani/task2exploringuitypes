package step_definitions;

import org.example.pageObject.DroppablePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DroppableSteps {

    private WebDriver driver = Hooks.driver;
    DroppablePage droppablePage = new DroppablePage(driver);

    @Given("User already on website jQueryui.com")
    public void userOnWebsiteJQuery() {
        Assert.assertTrue(droppablePage.verifyLandingPage());
    }

    @When("User clicks Droppable menu")
    public void userClicksDroppableMenu() {
        droppablePage.clickDroppableMenu();
    }

    @And("Perform drag and drop to the target")
    public void userPerformDragAndDrop() {
        droppablePage.dragAndDrop();
    }

    @Then("Dropped text will be showed up")
    public void droppedTextWillBeShowedUp() {
        droppablePage.verifyDroppedText();
    }

    @And("Boxs colour will change to yellow")
    public void boxColourWillChangeToYellow() {
        droppablePage.verifyDroppedColour();
    }

}
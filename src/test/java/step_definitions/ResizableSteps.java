package step_definitions;

import org.example.pageObject.ResizablePage;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ResizableSteps {

    private WebDriver driver = Hooks.driver;
    ResizablePage resizablePage = new ResizablePage(driver);

    @When("User clicks Resizable menu")
    public void userClicksResizableMenu() {
        resizablePage.clickResizableMenu();
    }

    @And("Perform resize the box by height {int} and width {int}")
    public void performResizeBox(int height, int width) {
        resizablePage.getSizeBeforeResize();
        resizablePage.resizeAction(height, width);
    }

    @Then("Validate size of the box is bigger")
    public void validateSizeOfBoxIsBigger() {
        resizablePage.getSizeAfterResize();
        resizablePage.verifyResizeSuccess();
    }

}

package step_definitions;

import org.example.pageObject.SortablePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SortableSteps {

    private WebDriver driver = Hooks.driver;
    SortablePage sortablePage = new SortablePage(driver);

    @When("User clicks Sortable menu")
    public void userClicksSortableMenu() {
        sortablePage.clickSortableMenu();
    }

    @And("Perform sorting item {string} to the item {string}")
    public void performSortingItemToOtherLoc(String item1, String item2) {
        sortablePage.orderBeforeSorting();
        sortablePage.performSorting(item1, item2);
    }

    @And("Also do sorting item {string} to the item {string}")
    public void alsoDoSortingItemToOtherItem(String loc1, String loc2) {
        sortablePage.performSorting(loc1, loc2);
        sortablePage.orderAfterSorting();
    }

    @Then("User verifies that the new item order is changed after the sorting action")
    public void userVerifiesNewItemOrder() {
        Assert.assertTrue(sortablePage.verifyNewItemOrder());
    }

}
package step_definitions;

import org.example.pageObject.TooltipPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TooltipSteps {

    private WebDriver driver = Hooks.driver;
    TooltipPage tooltipPage = new TooltipPage(driver);

    @When("User clicks Tooltip menu")
    public void userClicksTooltipMenu() {
        tooltipPage.clickTooltipMenu();
    }

    @And("Hover the mouse pointer to the your age box")
    public void hoverTheMousePointerToTheYourAge() {
        tooltipPage.hoverThePointerToYourAgeBox();
    }

    @Then("Validate text inside tooltip is {string}")
    public void validateTextInsideTooltip(String text) {
        Assert.assertEquals(text, tooltipPage.verifyTextInsideTooltip());
    }


















    
}

package step_definitions;

import org.example.pageObject.AccordionPage;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AccordionSteps {

    private WebDriver driver = Hooks.driver;
    AccordionPage accordionPage = new AccordionPage(driver);

    @When("User clicks Accordion menu")
    public void userClicksAccordionMenu() {
        accordionPage.clickAccordionMenu();
    }

    @And("Click {string} on accordion content panel")
    public void clickOnOfSectionInAccordion(String section) {
        accordionPage.clickAccordionSection(section);
    }

    @Then("Validate text is displayed")
    public void validateAccordionTextDisplayed() throws InterruptedException {
        accordionPage.verifyText();
    }

}
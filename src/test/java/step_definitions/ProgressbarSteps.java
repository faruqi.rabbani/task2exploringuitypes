package step_definitions;

import org.example.pageObject.ProgressbarPage;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ProgressbarSteps {

    private WebDriver driver = Hooks.driver;
    ProgressbarPage progressbarPage = new ProgressbarPage(driver);

    @When("User clicks Progressbar menu")
    public void userClicksProgressbarMenu() {
        progressbarPage.clickProgressbarMenu();
    }
    
    @And("Click download dialog")
    public void clickDownloadDialog() {
        progressbarPage.clickDownloadDialog();
    }

    @And("Click start download button")
    public void clickStartDownloadButton() {
        progressbarPage.clicksStartDownload();
    }

    @And("User validate {string} button is displayed when {string}")
    public void userValidateCancelDownloadButtonIsDisplayed(String cancel, String percent) {
        progressbarPage.verifyCancelTextAndPercentage(cancel, percent);
    }

    @Then("After download progress is finished, the {string} text will be displayed")
    public void afterDownloadProgressFinishedDisplayComplete(String complete) {
        progressbarPage.verifyCompleteText(complete);
    }

}

package step_definitions;

import org.example.pageObject.DatepickerPage;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DatepickerSteps {

    private WebDriver driver = Hooks.driver;
    DatepickerPage datepickerPage = new DatepickerPage(driver);

    @When("User clicks Datepicker menu")
    public void userClicksDatepickerMenu() {
        datepickerPage.clickDatepickerMenu();
    }

    @And("Clicks on date box")
    public void clicksOnDateBox() {
        datepickerPage.clickOnDateBox();
    }

    @And("User select date {string} on calendar pop up")
    public void userSelectDate(String date) {
        datepickerPage.selectDate(date);
    }

    @Then("Selected date box colour will change to blue")
    public void selectedDateBoxColourChangeToBlue() {
        datepickerPage.verifyDatepickerColour();
    }

}